# GitLab Cheatsheet

## GitLab SOS

Refer: [https://gitlab.com/gitlab-com/support/toolbox/gitlabsos](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos)


### Running GitLab SOS

```
/opt/gitlab/embedded/bin/git clone https://gitlab.com/gitlab-com/support/toolbox/gitlabsos.git && cd gitlabsos
./gitlabsos.rb
```

**or** execute directly without cloning:

```
curl https://gitlab.com/gitlab-com/support/toolbox/gitlabsos/raw/master/gitlabsos.rb | /opt/gitlab/embedded/bin/ruby
```